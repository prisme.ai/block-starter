import { useEffect } from 'react';
import { Schema } from '@prisme.ai/design-system';
import { useTranslation } from 'react-i18next';
import { BaseBlock, BlockComponent, useBlock } from '@prisme.ai/blocks';
import { BaseBlockConfig } from '@prisme.ai/blocks/lib/Blocks/types';

interface MyBlockConfig extends BaseBlockConfig {
  world: string;
}

type MyBlockProps = MyBlockConfig;

export const MyBlock = ({ className, world }: MyBlockProps) => {
  const { t } = useTranslation();

  return (
    <div className={`block-my-block ${className}`}>
      <div className="block-my-block__text">Hello {world || t('world')}!</div>
    </div>
  );
};

const schema: Schema = {
  type: 'object',
  properties: {
    foo: {
      type: 'string',
    },
  },
};

const defaultStyles = `:block {
  display: flex;
  flex: 1;
  flex-direction: column;
}
.block-my-block__text {
  display: flex;
  flex: 1;
  flex-direction: column;
  margin-right: 0.5rem;
}`;

export const MyBlockInContext: BlockComponent = ({ name, language }) => {
  const { config } = useBlock<MyBlockConfig>();
  const { i18n } = useTranslation();

  useEffect(() => {
    i18n.changeLanguage(language);
  }, [i18n, language]);

  return (
    <BaseBlock defaultStyles={defaultStyles}>
      <MyBlock {...config} />
    </BaseBlock>
  );
};

MyBlockInContext.schema = schema;
MyBlockInContext.styles = defaultStyles;

export default MyBlockInContext;
