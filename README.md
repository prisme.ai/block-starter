# Prisme.ai Block starter

This project is a scaffolding to start making Blocks for [Prisme.ai platform](https://prisme.ai)

## Usage

1. Fork this repo or copy its content.
2. Run `npm i`
3. Run `npm start`
4. Go to your [Prisme.ai](https://prisme.ai) workspace, edit its config by clicking "Edit source code" in the gear icon settings and add this in the `blocks` section (add it if it does not exist already):
```yaml
blocks:
  yourBlockSlug:
    name: Your Block name
    url: http://localhost:9090/main.js
```
6. Create a page, then see your Block in the list, add it to your page and it will be hot reloaded while you modify its code
5. 🚀 You can edit your component with React and Typescript and see it live 🎉

## Build

When your Block is ready to be shipped, you can run `npm run build`. You'll get a `main.js` file in the `dist` folder. Upload this file in your Prisme.ai Workspace as a new Block and then publish your Workspace as an App.  
Don't forget to replace the url of your localhost url by the final one in your Workspace config.
