/**
 * generates:
 *  - dist/main.js
 *  - dist/manifest.json
 *  - dist/webpack-bundle-analyzer-report.html
 */
const webpack = require('webpack');
const WebpackAssetsManifest = require('webpack-assets-manifest');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const { StatsPlugin } = require('./webpack-block-metadata-plugin');

module.exports = {
  plugins: [
    new webpack.EnvironmentPlugin({
      'process.env.NODE_ENV': process.env.NODE_ENV,
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      openAnalyzer: false,
      reportFilename: 'webpack-bundle-analyzer-report.html',
    }),
    new WebpackAssetsManifest({
      output: 'manifest.json',
    }),
    new StatsPlugin(),
    {
      apply(compiler) {
        compiler.hooks.thisCompilation.tap('Replace', (compilation) => {
          compilation.hooks.processAssets.tap(
            {
              name: 'Replace',
            },
            () => {
              // get the file main.js
              const file = compilation.getAsset('main.js');
              // update main.js with new content
              compilation.updateAsset(
                'main.js',
                new webpack.sources.RawSource(
                  file.source
                    .source()
                    .replace(
                      /import\s?\*\s?as\s? (\w+) from\s?"react"/,
                      'const $1 = __external.React'
                    )
                    .replace(
                      /import\s?\*\s?as\s? (\w+) from\s?"react-dom"/,
                      'const $1 = __external.ReactDom'
                    )
                    .replace(
                      /import\s?\*\s?as\s? (\w+) from\s?"@prisme.ai\/design-system"/,
                      'const $1 = __external.prismeaiDS'
                    )
                    .replace(
                      /import\s?\*\s?as\s? (\w+) from\s?"@prisme.ai\/sdk"/,
                      'const $1 = __external.prismeaiSDK'
                    )
                    .replace(
                      /import\s?\*\s?as\s? (\w+) from\s?"@prisme.ai\/blocks"/,
                      'const $1 = __external.prismeaiBlocks'
                    )
                )
              );
            }
          );
        });
      },
    },
  ],

  entry: {
    main: './src/index.ts',
  },
  experiments: {
    outputModule: true,
  },
  output: {
    library: {
      type: 'module',
    },
    filename: 'main.js',
  },
  externals: {
    react: 'react',
    'react-dom': 'react-dom',
    '@prisme.ai/design-system': '@prisme.ai/design-system',
    '@prisme.ai/sdk': '@prisme.ai/sdk',
    '@prisme.ai/blocks': '@prisme.ai/blocks',
  },
  module: {
    rules: [
      {
        test: /\.(png|jpg|gif|svg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: true,
            },
          },
        ],
      },
      {
        test: /\.(t|j)sx?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  resolve: {
    extensions: [
      '.ts', // Add typescript support
      '.tsx', // Add typescript + react support
      '.js', // Preserving webpack default
      '.jsx', // Preserving webpack default
      '.json', // Preserving webpack default
      '.css', // Preserving webpack default
    ],
    fallback: {
      crypto: false,
      stream: false,
    },
  },
};
