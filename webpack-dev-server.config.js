const webpack = require('webpack');
const config = require('./webpack.config');

/** @type import("webpack").Configuration */
module.exports = {
  ...config,
  devtool: 'eval-cheap-module-source-map',
  plugins: [new webpack.HotModuleReplacementPlugin(), ...config.plugins],
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers': [
        'X-Requested-With, content-type, Authorization',
        'X-Requested-With',
        'content-type',
        'Authorization',
        'sentry-trace',
      ],
    },
    hot: true,
    open: process.env.BROWSER !== 'none',
    port: 9090,
    static: {
      directory: `${__dirname}/dist`,
    },
  },
};
